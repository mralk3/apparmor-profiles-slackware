# apparmor-profiles-slackware

A collection of AppArmor profiles for Slackware Linux.

### Directions

1. You will need to recompile your kernel and enable apparmor.
2. Install apparmor using the SlackBuilds.org SlackBuild.
3. Copy the desired apparmor profiles to /etc/apparmor.d
4. Execute _aa-enforce profile.name_

#### Notes
* Restart unconfined services so they become confined.
* The firefox profile targets version 68.x.x ESR.
* dhcpcd and wpa_supplicant profiles assume rc.inet1
* dhcpcd and wpa_supplicant have not been tested with rc.networkmanager.
